Designed to be used in [Gitlab](https://gitlab.com) CI. To use standalone use `-v $(pwd):/home/knuth` to access to your local files.

For example:

```sh
docker run --rm -ti -v $(pwd):/home/knuth mmonga/texlive pdflatex foo.tex
```